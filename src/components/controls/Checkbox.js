import React from 'react';
import {
  FormControl,
  FormControlLabel,
  Checkbox as MuiCheckbox
} from '@material-ui/core';
import PropTypes from 'prop-types';

export default function Checkbox(props) {
  const {
    name,
    label,
    value,
    onChange
  } = props;

  const convertToDefEventPara = (names, values) => ({
    target: {
      names,
      values
    }
  });

  return (
    <FormControl>
      <FormControlLabel
        control={
          (
            <MuiCheckbox
              name={name}
              color="primary"
              checked={value}
              onChange={(e) => onChange(convertToDefEventPara(name, e.target.checked))}
            />
          )
        }
        label={label}
      />
    </FormControl>
  );
}
Checkbox.propTypes = {
  name: PropTypes.any,
  label: PropTypes.any,
  value: PropTypes.any,
  onChange: PropTypes.any,
};
