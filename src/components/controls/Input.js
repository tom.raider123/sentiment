import React from 'react';
import { TextField } from '@material-ui/core';
import PropTypes from 'prop-types';

export default function Input(props) {
  const {
    name,
    label,
    value,
    error = null,
    onChange,
    ...other
  } = props;
  return (
    <TextField
      variant="outlined"
      label={label}
      name={name}
      value={value}
      onChange={onChange}
      {...other}
      {...(error && { error: true, helperText: error })}
    />
  );
}
Input.propTypes = {
  name: PropTypes.any,
  label: PropTypes.any,
  value: PropTypes.any,
  error: PropTypes.any,
  onChange: PropTypes.any
};
