import React from 'react';
import {
  FormControl,
  InputLabel,
  Select as MuiSelect,
  MenuItem,
  FormHelperText
} from '@material-ui/core';
import PropTypes from 'prop-types';

export default function Select(props) {
  const {
    name,
    label,
    value,
    error = null,
    onChange,
    options
  } = props;

  return (
    <FormControl variant="outlined" {...(error && { error: true })}>
      <InputLabel>{label}</InputLabel>
      <MuiSelect label={label} name={name} value={value} onChange={onChange}>
        <MenuItem value="">None</MenuItem>
        {options.map((item) => (
          <MenuItem key={item.id} value={item.id}>
            {item.title}
          </MenuItem>
        ))}
      </MuiSelect>
      {error && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  );
}

Select.propTypes = {
  name: PropTypes.any,
  label: PropTypes.any,
  error: PropTypes.any,
  onChange: PropTypes.any,
  options: PropTypes.any,
  value: PropTypes.any
};
