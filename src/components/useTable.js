// import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import {
//   Table,
//   TableHead,
//   TableRow,
//   TableCell,
//   TablePagination,
//   TableSortLabel
// } from '@material-ui/core';

// const useTable = (records, headCells, filterFn) => {
//   const pages = [5, 10, 25];
//   const [page, setPage] = useState(0);
//   const [rowsPerPage, setRowsPerPage] = useState(pages[page]);
//   const [order, setOrder] = useState();
//   const [orderBy, setOrderBy] = useState();

//   //   const TblContainer = ({ children }) => (<Table>{children}</Table>);

//   const TblContainer = ({ children }) => {
//     return (
//       <Table>{children}</Table>
//     );
//   };

//   const TblHead = () => {
//     const handleSortRequest = (cellId) => {
//       const isAsc = orderBy === cellId && order === 'asc';
//       setOrder(isAsc ? 'desc' : 'asc');
//       setOrderBy(cellId);
//     };

//     return (
//       <TableHead>
//         <TableRow>
//           {headCells.map((headCell) => (
//             <TableCell
//               key={headCell.id}
//               sortDirection={orderBy === headCell.id ? order : false}
//             >
//               {headCell.disableSorting ? (
//                 headCell.label
//               ) : (
//                 <TableSortLabel
//                   active={orderBy === headCell.id}
//                   direction={orderBy === headCell.id ? order : 'asc'}
//                   onClick={() => {
//                     handleSortRequest(headCell.id);
//                   }}
//                 >
//                   {headCell.label}
//                 </TableSortLabel>
//               )}
//             </TableCell>
//           ))}
//         </TableRow>
//       </TableHead>
//     );
//   };

//   const handleChangePage = (event, newPage) => {
//     setPage(newPage);
//   };

//   const handleChangeRowsPerPage = (event) => {
//     setRowsPerPage(parseInt(event.target.value, 10));
//     setPage(0);
//   };

//   const TblPagination = () => (
//     <TablePagination
//       component="div"
//       page={page}
//       rowsPerPageOptions={pages}
//       rowsPerPage={rowsPerPage}
//       count={records.length}
//       onChangePage={handleChangePage}
//       onChangeRowsPerPage={handleChangeRowsPerPage}
//     />
//   );

//   function stableSort(array, comparator) {
//     const stabilizedThis = array.map((el, index) => [el, index]);
//     stabilizedThis.sort((a, b) => {
//       const sortOrder = comparator(a[0], b[0]);
//       if (sortOrder !== 0) return sortOrder;
//       return a[1] - b[1];
//     });
//     return stabilizedThis.map((el) => el[0]);
//   }

//   function descendingComparator(a, b, ordersBy) {
//     if (b[ordersBy] < a[ordersBy]) {
//       return -1;
//     }
//     if (b[ordersBy] > a[ordersBy]) {
//       return 1;
//     }
//     return 0;
//   }

//   function getComparator(descOrder, descOrderBy) {
//     return descOrder === 'desc'
//       ? (a, b) => descendingComparator(a, b, descOrderBy)
//       : (a, b) => -descendingComparator(a, b, descOrderBy);
//   }
//   const recordsAfterPagingAndSorting = () => {
//     return stableSort(
//       filterFn.fn(records),
//       getComparator(order, orderBy)
//     ).slice(page * rowsPerPage, (page + 1) * rowsPerPage);
//   };

//   return {
//     TblContainer,
//     TblHead,
//     TblPagination,
//     recordsAfterPagingAndSorting
//   };
// };

// TblContainer.propTypes = {
//   children: PropTypes.any,
// };

// export default useTable;
