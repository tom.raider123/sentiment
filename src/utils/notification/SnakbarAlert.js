// components/SuccessSnackbar.js or whatever you wanna call it
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { clearSnackbar } from '../../redux/actions/snackbarActions';

export default function SnackbarAlert() {
  const dispatch = useDispatch();
  const { successSnackbarOpen, successSnackbarMessage } = useSelector((state) => state.uiReducer);
  function handleClose() {
    dispatch(clearSnackbar());
  }
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      open={successSnackbarOpen}
      autoHideDuration={4000}
      onClose={handleClose}
      message={successSnackbarMessage}
      action={(
        <>
          <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
            <CloseIcon fontSize="small" />
          </IconButton>
        </>
      )}
    />
  );
}
