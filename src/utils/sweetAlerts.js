import Swal from 'sweetalert2';

export const processing = (message) => {
  return Swal.fire({
    title: 'Tekzee Sentiments',
    html: message,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    allowOutsideClick: false
  });
};
export const loading1 = () => {};
