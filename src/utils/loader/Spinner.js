import React from 'react';
import { BeatLoader } from 'react-spinners';

function Spinner() {
  return (
    <div>
      <BeatLoader size={30} color="#0000CD" />
    </div>
  );
}

export default Spinner;
