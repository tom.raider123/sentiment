import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Spinner from './Spinner';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#0000ffff',
    background: 'rgb(0 0 0 / 30%)'
  }
}));

export default function SimpleBackdrop({ value }) {
  const classes = useStyles();
  return (
    <div>
      <Backdrop className={classes.backdrop} open={value}>
        <Spinner size={40} color="inherit" />
      </Backdrop>
    </div>
  );
}
SimpleBackdrop.propTypes = {
  value: PropTypes.string
};
