import React from 'react';
import { Navigate } from 'react-router-dom';

import DashboardLayout from 'src/layouts/DashboardLayout';
import MainLayout from 'src/layouts/MainLayout';
import AccountView from 'src/views/account/AccountView';
import CustomerListView from 'src/views/customer/CustomerListView';
import CustomerView from 'src/views/customer/CustomerView';
import BatchListView from 'src/views/customer/CustomerBatchView';
import CustomerBatchStaticsView from 'src/views/customer/CustomerBatchStaticsView';
import DashboardView from 'src/views/reports/DashboardView';
import LoginView from 'src/views/auth/LoginView';
import NotFoundView from 'src/views/errors/NotFoundView';
import ProductListView from 'src/views/product/ProductListView';
import RegisterView from 'src/views/auth/RegisterView';
import SettingsView from 'src/views/settings/SettingsView';
import UploadView from 'src/views/upload/UploadsView';
import ResetPassword from 'src/views/auth/ResetPassword';
import ForgotPassword from 'src/views/auth/ForgotPassword';
import ActivationEmail from 'src/views/auth/ActivationEmail';
import TranslationView from 'src/views/customer/TranslationView';

const routes = (isLogged) => [
  {
    path: '/',
    element: !isLogged ? <MainLayout /> : <Navigate to="/app/dashboard" />,
    children: [
      { path: '/', element: <Navigate to="/login" /> },
      { path: '/login', element: <LoginView /> },
      { path: 'register', element: <RegisterView /> },
      { path: '/user/reset/:token', element: <ResetPassword /> },
      { path: 'forgetpassword', element: <ForgotPassword /> },
      { path: 'activate/:activationToken', element: <ActivationEmail /> },
      { path: '404', element: <NotFoundView /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: 'app',
    element: isLogged ? <DashboardLayout /> : <Navigate to="/login" />,
    children: [
      { path: 'account', element: <AccountView /> },
      { path: 'customers/:id', element: <CustomerListView /> },
      { path: 'batches/', element: <BatchListView /> },
      { path: 'statics/:id', element: <CustomerBatchStaticsView /> },
      { path: 'translation/:id', element: <TranslationView /> },
      { path: 'view/:id', element: <CustomerView /> },
      { path: 'dashboard', element: <DashboardView /> },
      { path: 'products', element: <ProductListView /> },
      { path: 'settings', element: <SettingsView /> },
      { path: 'upload', element: <UploadView /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];
export default routes;
