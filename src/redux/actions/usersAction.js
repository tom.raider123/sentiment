import axios from 'axios';
import ACTIONS from '../../constants/actionTypes';

// export const fetchAllUsers = async (token) => {
//   const res = await axios.get('/user/all_infor', {
//     headers: { Authorization: token }
//   });
//   return res;
// };

// export const updateProfile = (id, token, profile) => async (dispatch) => {
//   try {
//     const data = await axios.patch(`/user/update/${id}`, profile, {
//       headers: { Authorization: token }
//     });
//     dispatch({ type: ACTIONS.UPDATE_USER_PROFILE, payload: data });
//   } catch (error) {
//     console.log(error);
//   }
// };

export const updateProfile = (id, token, profile) => async (dispatch) => {
  dispatch({ type: ACTIONS.UPDATE_USER_PROFILE_REQUEST, payload: { id } });
  try {
    const data = await axios.patch(`/user/update/${id}`, profile, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.UPDATE_USER_PROFILE_SUCCESS, payload: data });
    window.location.reload();
  } catch (error) {
    dispatch({
      type: ACTIONS.UPDATE_USER_PROFILE_FAIL,
      payload:
        error.response && error.response.data.msg
          ? error.response.data.msg
          : error.message
    });
  }
};

export const updateAvatar = (id, token, avatar) => async (dispatch) => {
  try {
    dispatch({ type: ACTIONS.UPDATE_USER_AVATAR_REQUEST, payload: { id } });
    const data = await axios.patch(`/user/update_avatar/${id}`, avatar, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.UPDATE_USER_AVATAR_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: ACTIONS.UPDATE_USER_AVATAR_FAIL,
      payload:
        error.response && error.response.data.msg
          ? error.response.data.msg
          : error.message
    });
  }
};

export const dispatchGetAllUsers = (res) => {
  return {
    type: ACTIONS.GET_ALL_USERS,
    payload: res.data
  };
};
