import Axios from 'axios';
import ACTIONS from '../../constants/actionTypes';

export const speechToText = (id) => async (dispatch) => {
  dispatch({ type: ACTIONS.SPEECH_TO_TEXT_REQUEST, payload: { id } });
  try {
    const { data } = await Axios.post('/api/speech', { id });
    dispatch({ type: ACTIONS.SPEECH_TO_TEXT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: ACTIONS.SPEECH_TO_TEXT_FAIL,
      payload:
        error.response && error.response.data.msg
          ? error.response.data.msg
          : error.message
    });
  }
};
export const hello = () => {

};
