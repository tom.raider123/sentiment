import Axios from 'axios';
import ACTIONS from '../../constants/actionTypes';

export const userDashboard = (id, token) => async (dispatch) => {
  dispatch({ type: ACTIONS.GET_USER_DASHBOARD_REQUEST, payload: { id } });
  try {
    const { data } = await Axios.get(`/user/dashboard_info/${id}`, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.GET_USER_DASHBOARD_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: ACTIONS.GET_USER_DASHBOARD_FAIL,
      payload:
        error.response && error.response.data.msg
          ? error.response.data.msg
          : error.message
    });
  }
};
export const hello = () => {

};
