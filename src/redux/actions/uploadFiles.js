import axios from 'axios';
import ACTIONS from '../../constants/actionTypes';

export const getFiles = (batchId, token) => async (dispatch) => {
  try {
    dispatch({ type: ACTIONS.GET_USER_FILES_REQUEST, payload: batchId });
    const { data } = await axios.get(`/user/get_batch_files/${batchId}`, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.GET_USER_FILES_SUCCESS, payload: data });
  } catch (error) {
    const message = error.response && error.response.data.msg
      ? error.response.data.msg
      : error.message;
    dispatch({ type: ACTIONS.GET_USER_FILES_FAIL, payload: message });
  }
};
export const getUserBatches = (userId, token) => async (dispatch) => {
  try {
    dispatch({ type: ACTIONS.GET_USER_BATCHES_REQUEST, payload: userId });
    const { data } = await axios.get(`/user/get_user_batches/${userId}`, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.GET_USER_BATCHES__SUCCESS, payload: data });
  } catch (error) {
    const message = error.response && error.response.data.msg
      ? error.response.data.msg
      : error.message;
    dispatch({ type: ACTIONS.GET_USER_BATCHES__FAIL, payload: message });
  }
};

export const uploadFiles = (files, token, id) => async (dispatch) => {
  try {
    dispatch({ type: ACTIONS.USER_FILE_UPLOAD_REQUEST, payload: files });
    const data = await axios.post(`/user/upload_files/${id}`, files, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.USER_FILE_UPLOAD_SUCCESS, payload: data });
  } catch (error) {
    const message = error.response && error.response.data.msg
      ? error.response.data.msg
      : error.message;
    dispatch({ type: ACTIONS.USER_FILE_UPLOAD_FAIL, payload: message });
  }
};
