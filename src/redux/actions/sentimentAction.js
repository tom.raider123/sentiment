import Axios from 'axios';
import ACTIONS from '../../constants/actionTypes';

export const TextToSentiment = (id) => async (dispatch) => {
  dispatch({ type: ACTIONS.TEXT_TO_SENTIMENTS_REQUEST, payload: { id } });
  try {
    const { data } = await Axios.post('/api/sentiment', { id });
    dispatch({ type: ACTIONS.TEXT_TO_SENTIMENTS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: ACTIONS.TEXT_TO_SENTIMENTS_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
    });
  }
};

export const getFilesById = (id, token) => async (dispatch) => {
  dispatch({ type: ACTIONS.GET_FILE_REQUEST, payload: { id } });
  try {
    const data = await Axios.get(`/api/getFile/${id}`, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.GET_FILE_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: ACTIONS.GET_FILE_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
    });
  }
};

export const getTranslationById = (id, token) => async (dispatch) => {
  dispatch({ type: ACTIONS.GET_FILE_TRANSLATION_REQUEST, payload: { id } });
  try {
    const data = await Axios.get(`/api/getTranslation//${id}`, {
      headers: { Authorization: token }
    });
    dispatch({ type: ACTIONS.GET_USER_TRANSLATION_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: ACTIONS.GET_USER_TRANSLATION_FAIL,
      payload:
        error.response && error.response.data.msg
          ? error.response.data.msg
          : error.message
    });
  }
};
