import ACTIONS from '../../constants/actionTypes';

export const showSuccessSnackbar = (message) => {
  return (dispatch) => {
    dispatch({ type: ACTIONS.SNACKBAR_SUCCESS, message });
  };
};
export const clearSnackbar = () => {
  return (dispatch) => {
    dispatch({ type: ACTIONS.SNACKBAR_CLEAR });
  };
};
