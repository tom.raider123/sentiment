import { combineReducers } from 'redux';
import auth from './authReducer';
import token from './tokenReducer';
import { getUserFilesReducer, filesUploadReducer, getUserBatchesReducer } from './fileUpload';
import { updateUserAvatar, updateUserProfile } from './usersReducer';
import { speechToText } from './speechToText';
import { TextToSentiment, getFilesById, getTranslationById } from './textToSentiments';
import { userDashboard } from './dashboardReducer';
import uiReducer from './uiReducer';

const reducers = combineReducers({
  getUserFiles: getUserFilesReducer,
  uploadUserFiles: filesUploadReducer,
  getUserBatches: getUserBatchesReducer,
  auth,
  token,
  speechToText,
  TextToSentiment,
  getFilesById,
  updateUserProfile,
  updateUserAvatar,
  userDashboard,
  getTranslationById,
  uiReducer
});
export default reducers;
