import ACTIONS from '../../constants/actionTypes';

const uiReducer = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.SNACKBAR_SUCCESS:
      return {
        ...state,
        successSnackbarOpen: true,
        successSnackbarMessage: action.message
      };
    case ACTIONS.SNACKBAR_CLEAR:
      return {
        ...state,
        successSnackbarOpen: false,
        errorSnackbarOpen: false,
        infoSnackbarOpen: false
      };
    default:
      return state;
  }
};

export default uiReducer;
