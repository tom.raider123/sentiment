import ACTIONS from '../../constants/actionTypes';

export const userDashboard = (state = { dashboard: {} }, action) => {
  switch (action.type) {
    case ACTIONS.GET_USER_DASHBOARD_REQUEST:
      return { loading: true };
    case ACTIONS.GET_USER_DASHBOARD_SUCCESS:
      return { loading: false, dashboard: action.payload };
    case ACTIONS.GET_USER_DASHBOARD_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const hello = () => {};
