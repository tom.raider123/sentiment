import ACTIONS from '../../constants/actionTypes';

export const updateUserProfile = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.UPDATE_USER_PROFILE_REQUEST:
      return { loading: true };
    case ACTIONS.UPDATE_USER_PROFILE_SUCCESS:
      return { loading: false, status: action.payload };
    case ACTIONS.UPDATE_USER_PROFILE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const updateUserAvatar = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.UPDATE_USER_AVATAR_REQUEST:
      return { loading: true };
    case ACTIONS.UPDATE_USER_AVATAR_SUCCESS:
      return { loading: false, status: action.payload };
    case ACTIONS.UPDATE_USER_AVATAR_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
