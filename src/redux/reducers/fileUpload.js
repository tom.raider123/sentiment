import ACTIONS from '../../constants/actionTypes';

export const getUserFilesReducer = (
  state = { loading: true, files: [] },
  action
) => {
  switch (action.type) {
    case ACTIONS.GET_USER_FILES_REQUEST:
      return { loading: true };
    case ACTIONS.GET_USER_FILES_SUCCESS:
      return {
        loading: false,
        files: action.payload.files,
      };
    case ACTIONS.USER_FILE_UPLOAD_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getUserBatchesReducer = (
  state = { loading: true, batches: [] },
  action
) => {
  switch (action.type) {
    case ACTIONS.GET_USER_BATCHES_REQUEST:
      return { loading: true };
    case ACTIONS.GET_USER_BATCHES__SUCCESS:
      return {
        loading: false,
        batches: action.payload.batches,
      };
    case ACTIONS.GET_USER_BATCHES__FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const filesUploadReducer = (state = { uploads: {} }, action) => {
  switch (action.type) {
    case ACTIONS.USER_FILE_UPLOAD_REQUEST:
      return { loading: true };
    case ACTIONS.USER_FILE_UPLOAD_SUCCESS:
      return {
        ...state,
        uploads: action.payload,
        loading: false,
      };
    case ACTIONS.USER_FILE_UPLOAD_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
