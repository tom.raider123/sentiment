import ACTIONS from '../../constants/actionTypes';

export const speechToText = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.SPEECH_TO_TEXT_REQUEST:
      return { loading: true };
    case ACTIONS.SPEECH_TO_TEXT_SUCCESS:
      return { loading: false, success: true };
    case ACTIONS.SPEECH_TO_TEXT_FAIL:
      return { loading: false, error: action.payload };
    case ACTIONS.SPEECH_TO_TEXT_RESET:
      return {};
    default:
      return state;
  }
};

export const hello = () => {};
