import ACTIONS from '../../constants/actionTypes';

export const TextToSentiment = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.TEXT_TO_SENTIMENTS_REQUEST:
      return { loading: true };
    case ACTIONS.TEXT_TO_SENTIMENTS_SUCCESS:
      return { loading: false, success: true };
    case ACTIONS.TEXT_TO_SENTIMENTS_FAIL:
      return { loading: false, error: action.payload };
    case ACTIONS.TEXT_TO_SENTIMENTS_RESET:
      return {};
    default:
      return state;
  }
};

export const getFilesById = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.GET_FILE_REQUEST:
      return { loading: true };
    case ACTIONS.GET_FILE_SUCCESS:
      return { loading: false, file: action.payload };
    case ACTIONS.GET_FILE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getTranslationById = (state = { translation: {} }, action) => {
  switch (action.type) {
    case ACTIONS.GET_FILE_TRANSLATION_REQUEST:
      return { loading: true };
    case ACTIONS.GET_USER_TRANSLATION_SUCCESS:
      return { loading: false, translation: action.payload };
    case ACTIONS.GET_USER_TRANSLATION_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
