import 'react-perfect-scrollbar/dist/css/styles.css';
import React, { useEffect } from 'react';
// import { useRoutes } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'src/components/GlobalStyles';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { useRoutes } from 'react-router';
import 'src/mixins/chartjs';
import theme from 'src/theme';
// import routes from 'src/Routes';
import routes from './Routes';
import { dispatchLogin, fetchUser, dispatchGetUser } from './redux/actions/authAction';
import SnakbarAlert from './utils/notification/SnakbarAlert';

const App = () => {
  // const routing = useRoutes(routes);
  const dispatch = useDispatch();
  const token = useSelector((state) => state.token);
  const auth = useSelector((state) => state.auth);
  useEffect(() => {
    const firstLogin = localStorage.getItem('firstLogin');
    if (firstLogin) {
      const getToken = async () => {
        const res = await axios.post('/user/refresh_token', null);
        dispatch({ type: 'GET_TOKEN', payload: res.data.access_token });
      };
      getToken();
    }
  }, [auth.isLogged, dispatch]);

  const routing = useRoutes(routes(auth.isLogged));

  useEffect(() => {
    if (token) {
      const getUser = () => {
        dispatch(dispatchLogin());
        return fetchUser(token).then((res) => {
          dispatch(dispatchGetUser(res));
        });
      };
      getUser();
    }
  }, [token, dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <SnakbarAlert />
      {routing}
    </ThemeProvider>
  );
};

export default App;
