import React from 'react';
// import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';
import AudiotrackIcon from '@material-ui/icons/Audiotrack';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
  avatar: {
    backgroundColor: colors.green[600],
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.green[900]
  },
  differenceValue: {
    color: colors.green[900],
    marginRight: theme.spacing(1)
  }
}));

const TotalCustomers = (data) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const handleClick = () => {
    navigate('/app/customers');
  };

  return (
    <Card onClick={handleClick}>
      <CardContent>
        <Grid
          container
          justify="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              TOTAL BATCHES
            </Typography>
            <Typography
              color="textPrimary"
              variant="h3"
            >
              {data && data.count}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <AudiotrackIcon />
            </Avatar>
          </Grid>
        </Grid>
        <Box
          mt={2}
          display="flex"
          alignItems="center"
        >
          <Typography
            className={classes.differenceValue}
            variant="body2"
          >
            Total uploaded
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

// TotalCustomers.propTypes = {
//   className: PropTypes.string
// };

export default TotalCustomers;
