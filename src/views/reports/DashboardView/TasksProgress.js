import React from 'react';
// import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
  makeStyles,
  colors
} from '@material-ui/core';
// import InsertChartIcon from '@material-ui/icons/InsertChartOutlined';
import CropRotateIcon from '@material-ui/icons/CropRotate';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
  avatar: {
    backgroundColor: colors.orange[600],
    height: 56,
    width: 56
  },
  differenceValue: {
    color: colors.orange[900],
    marginRight: theme.spacing(1)
  }
}));

const TasksProgress = (data) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const handleClick = () => {
    navigate('/app/customers');
  };

  return (
    <Card onClick={handleClick}>
      <CardContent>
        <Grid
          container
          justify="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              TOTAL CONVERTED
            </Typography>
            <Typography
              color="textPrimary"
              variant="h3"
            >
              {data && data.count}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <CropRotateIcon />
            </Avatar>
          </Grid>
        </Grid>
        <Box
          mt={2}
          display="flex"
          alignItems="center"
        >
          <Typography
            className={classes.differenceValue}
            variant="body2"
          >
            Total converted audios
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

// TasksProgress.propTypes = {
//   className: PropTypes.string
// };

export default TasksProgress;
