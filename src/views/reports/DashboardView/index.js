import React, { useEffect } from 'react';
import {
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Page from 'src/components/Page';
import Budget from './Budget';
import TasksProgress from './TasksProgress';
import TotalCustomers from './TotalCustomers';
import TotalProfit from './TotalProfit';
import TrafficByDevice from './TrafficByDevice';
import { userDashboard } from '../../../redux/actions/dashboardAction';
import SimpleBackdrop from '../../../utils/loader/BackDrop';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const Dashboard = () => {
  const classes = useStyles();
  const dashboardDate = useSelector((state) => (state.userDashboard));
  const {
    loading,
    error,
    dashboard,
  } = dashboardDate;
  const auth = useSelector((state) => state.auth);
  const token = useSelector((state) => state.token);
  const { user } = auth;
  const dispatch = useDispatch();
  const chart = {
    converted: dashboard && dashboard.totalConverted,
    analysed: dashboard && dashboard.totalAnalysis,
  };
  useEffect(() => {
    dispatch(userDashboard(user._id, token));
  }, [dispatch, user._id, token]);

  return (
    <Page
      className={classes.root}
      title="Dashboard"
    >
      {loading && <SimpleBackdrop value={loading} />}
      {error && error}
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <Budget count={dashboard && dashboard.totalAudioToday} />
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <TotalCustomers count={dashboard && dashboard.totalAudio} />
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <TasksProgress count={dashboard && dashboard.totalConverted} />
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <TotalProfit count={dashboard && dashboard.totalAnalysis} />
          </Grid>
          <Grid
            item
            lg={4}
            md={6}
            xl={3}
            xs={12}
          >
            {dashboard && dashboard.totalConverted > 0 ? <TrafficByDevice chart={chart} /> : ''}
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default Dashboard;
