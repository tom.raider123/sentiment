import React from 'react';
import { Pie } from 'react-chartjs-2';
import PropTypes from 'prop-types';
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  colors,
  useTheme
} from '@material-ui/core';

const TrafficByDevice = (props) => {
  const theme = useTheme();
  const { chart } = props;
  const speechToTextCount = chart ? chart.converted : 0;
  const textToSentimentsCount = chart ? chart.analysed : 0;

  const data = {
    datasets: [
      {
        data: [speechToTextCount, textToSentimentsCount],
        backgroundColor: [
          colors.indigo[500],
          colors.orange[600]
        ],
        borderWidth: 1,
        borderColor: colors.common.white,
        hoverBorderColor: colors.common.white
      }
    ],
    labels: ['Speech to text', 'Sentiment analysis']
  };

  const options = {
    animation: false,
    layout: { padding: 0 },
    maintainAspectRatio: false,
    responsive: true,
    tooltips: {
      backgroundColor: theme.palette.background.default,
      bodyFontColor: theme.palette.text.secondary,
      borderColor: theme.palette.divider,
      borderWidth: 1,
      enabled: true,
      footerFontColor: theme.palette.text.secondary,
      intersect: false,
      mode: 'index',
      titleFontColor: theme.palette.text.primary
    }
  };

  return (
    <Card>
      <CardHeader />
      <Divider />
      <CardContent>
        <Box
          height={300}
          position="relative"
        >
          <Pie
            data={data}
            options={options}
          />
        </Box>
      </CardContent>
    </Card>
  );
};

TrafficByDevice.propTypes = {
  chart: PropTypes.string,
};

export default TrafficByDevice;
