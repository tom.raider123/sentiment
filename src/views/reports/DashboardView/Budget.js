import React from 'react';
import { useNavigate } from 'react-router-dom';
// import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';
import MoneyIcon from '@material-ui/icons/Money';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
  avatar: {
    backgroundColor: colors.red[600],
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.red[900]
  },
  differenceValue: {
    color: colors.red[900],
    marginRight: theme.spacing(1)
  }
}));

const Budget = (data) => {
  const classes = useStyles();
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/app/customers');
  };

  return (
    <Card onClick={handleClick}>
      <CardContent>
        <Grid
          container
          justify="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              TODAY BATCHES
            </Typography>
            <Typography
              color="textPrimary"
              variant="h3"
            >
              {data && data.count}
            </Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <MoneyIcon />
            </Avatar>
          </Grid>
        </Grid>
        <Box
          mt={2}
          display="flex"
          alignItems="center"
        >
          <Typography
            className={classes.differenceValue}
            variant="body2"
          >
            Total uploads today
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

// Budget.propTypes = {
//   data: PropTypes.number,
// };

export default Budget;
