import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  CardActions,
  Button,
} from '@material-ui/core';
import { getTranslationById } from '../../../redux/actions/sentimentAction';
import SimpleBackdrop from '../../../utils/loader/BackDrop';

function TextView() {
  const token = useSelector((state) => state.token);
  const translationText = useSelector((state) => state.getTranslationById);
  const { translation, loading, error } = translationText;
  const { id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    if (id) {
      dispatch(getTranslationById(id, token));
    }
  }, [dispatch, token, id]);

  return (
    <Card>
      <CardHeader subheader="Translated text" title="Sentiment analysis" />
      <Divider />
      {loading && <SimpleBackdrop value={loading} />}
      {error && error}
      <CardContent>
        <Grid>
          {translation && translation.data}
        </Grid>
      </CardContent>
      <CardActions>
        <Button onClick={() => navigate(-1)} size="small">Back</Button>
      </CardActions>
    </Card>
  );
}

export default TextView;
