import React, { useEffect } from 'react';
import { Grid, makeStyles, Paper } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import BarChart from '../../charts/batchCharts/barChart';
import LineChart from '../../charts/batchCharts/lineChart';
import PiChart from '../../charts/batchCharts/piChart';
import DoughnutChart from '../../charts/batchCharts/doughnutChart';
import { getFiles } from '../../../redux/actions/uploadFiles';
import SimpleBackdrop from '../../../utils/loader/BackDrop';
import { showSuccessSnackbar } from '../../../redux/actions/snackbarActions';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary
  }
}));

function BatchStatics() {
  const classes = useStyles();
  const fileList = useSelector((state) => state.getUserFiles);
  const token = useSelector((state) => state.token);
  const { id } = useParams();
  const { loading, error, files } = fileList;
  const dispatch = useDispatch();
  useEffect(() => {
    if (id) {
      dispatch(getFiles(id, token));
    }
  }, [dispatch, id, token]);
  return (
    <>
      {loading && <SimpleBackdrop value={loading} />}
      {error && dispatch(showSuccessSnackbar(error))}
      {files && (
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <BarChart staticsData={files} />
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <LineChart staticsData={files} />
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <PiChart staticsData={files} />
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <DoughnutChart staticsData={files} />
            </Paper>
          </Grid>
        </Grid>
      )}
    </>
  );
}

export default BatchStatics;
