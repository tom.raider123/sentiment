import React from 'react';
import {
  Box,
  Container,
  makeStyles,
  Grid,
} from '@material-ui/core';
import Page from 'src/components/Page';
import CustomerView from './CustomerView';
import SentimentDoughnut from '../../charts/sentimentDoughnut';
// import SentimentBar from '../../charts/sentimentBar';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));
const CustomerDetailView = () => {
  const classes = useStyles();
  return (
    <Page className={classes.root} title="Customers">
      <Container maxWidth={false}>
        <Box mt={3} mb={3}>
          <CustomerView />
        </Box>
        <Grid container spacing={3}>
          <Grid item lg={6} sm={6} xl={6} xs={12}>
            <SentimentDoughnut />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default CustomerDetailView;
