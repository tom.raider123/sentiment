import React, { useEffect } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import Tooltip from '@material-ui/core/Tooltip';
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  CardActions,
  Button,
} from '@material-ui/core';
import Chip from '@material-ui/core/Chip';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ThumbsUpDownIcon from '@material-ui/icons/ThumbsUpDown';
import { getFilesById } from '../../../redux/actions/sentimentAction';
import SimpleBackdrop from '../../../utils/loader/BackDrop';

const CustomerView = () => {
  const token = useSelector((state) => state.token);
  const details = useSelector((state) => state.getFilesById);
  const { loading, error, file } = details;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();
  useEffect(() => {
    if (id) {
      dispatch(getFilesById(id, token));
    }
  }, [dispatch, token, id]);

  return (
    <Card>
      <CardHeader subheader="Analysis details" title="Sentiment analysis" />
      <Divider />
      {loading && <SimpleBackdrop value={loading} />}
      {error && error}
      <CardContent>
        <Grid>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Result</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>Score</TableCell>
                {file && (
                <TableCell>
                  <Chip label={file && file.data.score} color="secondary" />
                  {file.data.score > 0 && (
                  <Chip
                    icon={(<ThumbUpIcon />)}
                    label="Positive"
                    color="secondary"
                  />
                  )}
                  {file.data.score < 0 && (
                  <Chip
                    icon={(<ThumbDownIcon />)}
                    label="Negative"
                    color="secondary"
                  />
                  )}
                  {file.data.score === 0 && (
                  <Chip
                    icon={(<ThumbsUpDownIcon />)}
                    label="Neutral"
                    color="secondary"
                  />
                  )}
                </TableCell>
                )}
              </TableRow>
              <TableRow>
                <TableCell>Comparative</TableCell>
                <TableCell><Chip label={file && file.data.comparative} color="secondary" /></TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Positive words</TableCell>
                <TableCell>
                  { file && file.data.positive.map((possenti, index = uuidv4()) => (
                    <Chip key={index} label={possenti} color="secondary" />)) }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Negative words</TableCell>
                <TableCell>
                  { file && file.data.negative.map((negsenti, index = uuidv4()) => (
                    <Chip key={index} label={negsenti} color="secondary" />)) }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>View full translation</TableCell>
                <TableCell>
                  <Tooltip title="Click here to view full translation.." placement="top">
                    <Link to={`/app/translation/${id && id}`}>
                      <AssignmentIcon color="secondary" />
                    </Link>
                  </Tooltip>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Grid>
      </CardContent>
      <CardActions>
        <Button onClick={() => navigate(-1)} size="small">Back</Button>
      </CardActions>
    </Card>
  );
};

export default CustomerView;
