import React, { useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import { Link } from 'react-router-dom';
import Chip from '@material-ui/core/Chip';
import SettingsIcon from '@material-ui/icons/Settings';
import BarChartSharpIcon from '@material-ui/icons/BarChartSharp';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import VisibilityIcon from '@material-ui/icons/Visibility';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import moment from 'moment';
import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Button,
  // makeStyles,
} from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import Search from './Search';
import { getUserBatches } from '../../../redux/actions/uploadFiles';
import SimpleBackdrop from '../../../utils/loader/BackDrop';
import { showSuccessSnackbar } from '../../../redux/actions/snackbarActions';
// const useStyles = makeStyles((theme) => ({
//   root: {},
//   avatar: {
//     marginRight: theme.spacing(2)
//   },
// }));

const BatchList = () => {
  // const classes = useStyles();
  const batchesList = useSelector((state) => state.getUserBatches);
  const auth = useSelector((state) => state.auth);
  const token = useSelector((state) => state.token);
  const { user } = auth;
  const {
    loading,
    error,
    batches,
  } = batchesList;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserBatches(user._id, token));
  }, [dispatch, user._id, token, getUserBatches]);

  const pages = [5, 10, 25];
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(pages[page]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const [search, setSearch] = useState('');

  return (

    <Card>
      {loading && <SimpleBackdrop value={loading} />}
      {error && dispatch(showSuccessSnackbar(error))}
      <Box maxWidth={500} p={3}>
        <Search
          onSearch={(value) => {
            setSearch(value);
            // setCurrentPage(1);
          }}
        />
      </Box>
      {search}
      <PerfectScrollbar>
        <Box minWidth={1050}>
          {!error && (
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Added</TableCell>
                <TableCell>Batch Name</TableCell>
                <TableCell>Converted</TableCell>
                <TableCell>Analysed</TableCell>
                <TableCell>Positive</TableCell>
                <TableCell>Negative</TableCell>
                <TableCell>Neutral</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {batches && batches.slice(page * rowsPerPage,
                page * rowsPerPage + rowsPerPage).map((data, id = uuidv4()) => (
                  <TableRow hover key={id}>
                    <TableCell>
                      {moment(data.createdAt).format('DD-MM-YYYY')}
                    </TableCell>
                    <TableCell>
                      {data.batchName}
                    </TableCell>
                    <TableCell>
                      <Tooltip title="Total converted recordings in this batch." placement="top">
                        <Chip label={data.files[0].converted} color="secondary" size="small" icon={<SettingsIcon />} />
                      </Tooltip>
                    </TableCell>
                    <TableCell>
                      <Tooltip title="Total analysed recordings in this batch." placement="top">
                        <Chip label={data.files[0].analysed} color="secondary" size="small" icon={<BarChartSharpIcon />} />
                      </Tooltip>
                    </TableCell>
                    <TableCell>
                      <Tooltip title="Total positive recordings in this batch." placement="top">
                        <Chip label={data.files[0].positive} color="secondary" size="small" icon={<SentimentVerySatisfiedIcon />} />
                      </Tooltip>
                    </TableCell>
                    <TableCell>
                      <Tooltip title="Total negative recordings in this batch." placement="top">
                        <Chip label={data.files[0].negative} color="secondary" size="small" icon={<SentimentVeryDissatisfiedIcon />} />
                      </Tooltip>
                    </TableCell>
                    <TableCell>
                      <Tooltip title="Total neutral recordings in this batch." placement="top">
                        <Chip label={data.files[0].neutral} color="secondary" size="small" icon={<SentimentSatisfiedIcon />} />
                      </Tooltip>
                    </TableCell>
                    <TableCell>
                      <Tooltip title="Click here to view recordings." placement="top">
                        <Link to={`/app/customers/${data._id}`}>
                          <Button>
                            <VisibilityIcon color="secondary" />
                          </Button>
                        </Link>
                      </Tooltip>
                      <Tooltip title="Click here to view full batch analysis." placement="top">
                        <Link to={`/app/statics/${data._id}`}>
                          <Button>
                            <ShowChartIcon color="secondary" />
                          </Button>
                        </Link>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
              ))}
            </TableBody>
          </Table>
          )}
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        page={page}
        rowsPerPageOptions={pages}
        rowsPerPage={rowsPerPage}
        count={batches ? batches.length : 0}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Card>
  );
};

export default BatchList;
