import React from 'react';
import {
  Box,
  Container,
  makeStyles,

} from '@material-ui/core';
import Page from 'src/components/Page';
import BatchList from './BatchList';
import Toolbar from './Toolbar';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));
const BatchListView = () => {
  const classes = useStyles();
  return (
    <Page
      className={classes.root}
      title="Customers"
    >
      <Container maxWidth={false}>
        <Toolbar />
        <Box mt={3}>
          <BatchList />
        </Box>
      </Container>
    </Page>
  );
};

export default BatchListView;
