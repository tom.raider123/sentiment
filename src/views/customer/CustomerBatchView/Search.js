import React, { useState } from 'react';
import {
  TextField,
  InputAdornment,
  SvgIcon
  // makeStyles,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Search as SearchIcon } from 'react-feather';

const Search = ({ onSearch }) => {
  const [search, setSearch] = useState('');

  const onInputChange = (value) => {
    setSearch(value);
    onSearch(value);
  };
  return (
    <TextField
      fullWidth
      placeholder="Search batches"
      variant="outlined"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SvgIcon fontSize="small" color="action">
              <SearchIcon />
            </SvgIcon>
          </InputAdornment>
        )
      }}
      value={search}
      onChange={(e) => onInputChange(e.target.value)}
    />
  );
};
Search.propTypes = {
  onSearch: PropTypes.string,
};

export default Search;
