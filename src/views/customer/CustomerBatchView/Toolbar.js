import React from 'react';
// import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
// import clsx from 'clsx';
import {
  Box,
  Button,
  // Card,
  // CardContent,
  // TextField,
  // InputAdornment,
  // SvgIcon,
  makeStyles
} from '@material-ui/core';
// import { Search as SearchIcon } from 'react-feather';

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  }
}));

const Toolbar = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const handleRedirect = () => {
    navigate('/app/upload');
  };
  // const [search, setSearch] = useState('');

  // const onInputChange = (value) => {
  //   setSearch(value);
  // };

  return (
    <div classesName={classes.root}>
      <Box display="flex" justifyContent="flex-end">
        <Button color="primary" variant="contained" onClick={handleRedirect}>
          Add Files
        </Button>
      </Box>
      {/* <Box mt={3}>
        <Card>
          <CardContent>
            <Box maxWidth={500}>
              <TextField
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon fontSize="small" color="action">
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  )
                }}
                placeholder="Search batches"
                variant="outlined"
                value={search}
                onChange={(e) => onInputChange(e.target.value)}
              />
              {search}
            </Box>
          </CardContent>
        </Card>
      </Box> */}
    </div>
  );
};

// Toolbar.propTypes = {
//   onSearch: PropTypes.func
// };

export default Toolbar;
