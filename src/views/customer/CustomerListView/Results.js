import React, { useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import AudioPlayer from 'material-ui-audio-player';
import moment from 'moment';
import {
  createMuiTheme,
  ThemeProvider,
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Button,
  makeStyles,
  colors,
} from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAlt';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { getFiles } from '../../../redux/actions/uploadFiles';
import { speechToText } from '../../../redux/actions/speechAction';
import ACTIONS from '../../../constants/actionTypes';
import { TextToSentiment } from '../../../redux/actions/sentimentAction';
import SimpleBackdrop from '../../../utils/loader/BackDrop';
import { showSuccessSnackbar } from '../../../redux/actions/snackbarActions';
import Search from './Search';

const muiTheme = createMuiTheme({});
const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2)
  },
}));

const Results = () => {
  const [search, setSearch] = useState('');
  const { id } = useParams();
  const classes = useStyles();
  const fileList = useSelector((state) => state.getUserFiles);
  const auth = useSelector((state) => state.auth);
  const token = useSelector((state) => state.token);
  const { user } = auth;
  const {
    loading,
    error,
    files,
    // page,
    // pages
  } = fileList;
  const dispatch = useDispatch();

  //
  const audioToText = useSelector((state) => state.speechToText);
  const {
    loading: loadingAudioToText,
    error: errorAudioToText,
    success: successAudioToText,
  } = audioToText;
  const toSentiments = useSelector((state) => state.TextToSentiment);
  const {
    loading: loadingToSentiments,
    error: errorToSentiments,
    success: successToSentiments,
  } = toSentiments;
  //
  useEffect(() => {
    if (successAudioToText) {
      dispatch({ type: ACTIONS.SPEECH_TO_TEXT_RESET });
      dispatch(showSuccessSnackbar('Converted Successfully'));
    }
    if (successToSentiments) {
      dispatch({ type: ACTIONS.TEXT_TO_SENTIMENTS_RESET });
      dispatch(showSuccessSnackbar('Analysed Successfully'));
    }
    if (id) {
      dispatch(getFiles(id, token));
    }
  }, [dispatch, id, user._id, token, successAudioToText, successToSentiments]);

  const handleSpeechToText = (e) => {
    dispatch(speechToText(e));
  };
  const handleSentiments = (e) => {
    dispatch(TextToSentiment(e));
  };
  const pages = [5, 10, 25];
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(pages[page]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const searchQuery = (rows) => {
    const columns = rows[0] && Object.keys(rows[0]);
    return rows.filter(
      (row) => columns.some((column) => row[column].toString().toLowerCase().indexOf(search) > -1)
    );
  };

  return (

    <Card>
      {loading && <SimpleBackdrop value={loading} />}
      {error && dispatch(showSuccessSnackbar(error))}
      {loadingAudioToText && <SimpleBackdrop value={loadingAudioToText} />}
      {errorAudioToText && dispatch(showSuccessSnackbar(errorAudioToText))}
      {loadingToSentiments && <SimpleBackdrop value={loadingToSentiments} />}
      {errorToSentiments && dispatch(showSuccessSnackbar(errorToSentiments))}
      <Box maxWidth={500} p={3}>
        <Search
          onSearch={(value) => {
            setSearch(value);
            // setCurrentPage(1);
          }}
        />
      </Box>
      <PerfectScrollbar>
        <Box minWidth={1050}>
          {!error && (
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Added</TableCell>
                <TableCell>Audio</TableCell>
                <TableCell>Speech</TableCell>
                <TableCell>Sentiment</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {files && searchQuery(files).slice(page * rowsPerPage,
                page * rowsPerPage + rowsPerPage).map((data, unique = uuidv4()) => (
                  <TableRow hover key={unique}>
                    <TableCell>
                      {moment(data.createdAt).format('DD-MM-YYYY')}
                    </TableCell>
                    <TableCell>
                      <ThemeProvider theme={muiTheme}>
                        <AudioPlayer
                          variation="primary"
                          download="true"
                          src={data.audiourl}
                          elevation={0}
                          width="350px"
                          spacing={1}
                        />
                      </ThemeProvider>
                    </TableCell>
                    <TableCell>
                      {!data.texturl ? (
                        <Tooltip title="Click here to convert this audio to text." placement="top">
                          <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            startIcon={<RecordVoiceOverIcon />}
                            onClick={() => handleSpeechToText(data._id)}
                          />
                        </Tooltip>
                      ) : (
                        <Tooltip title="This file already been converted." placement="top">
                          <Button>Converted</Button>
                        </Tooltip>
                      )}
                    </TableCell>
                    <TableCell>
                      {data.texturl && !data.sentiments ? (
                        <Tooltip title="Click here for sentiment analysis." placement="top">
                          <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            startIcon={<SentimentSatisfiedAltIcon />}
                            onClick={() => handleSentiments(data._id)}
                          />
                        </Tooltip>
                      ) : ''}
                      {data.sentiments.score < 0 && (
                        <Tooltip title="Negative" placement="top">
                          <SentimentVeryDissatisfiedIcon style={{ color: colors.red[500] }} />
                        </Tooltip>
                      )}
                      {data.sentiments.score > 0 && (
                        <Tooltip title="Positive" placement="top">
                          <InsertEmoticonIcon style={{ color: colors.green[500] }} />
                        </Tooltip>
                      )}
                      {data.sentiments.score === 0 && (
                        <Tooltip title="Neutral" placement="top">
                          <SentimentSatisfiedIcon style={{ color: colors.yellow[500] }} />
                        </Tooltip>
                      )}
                    </TableCell>
                    <TableCell>
                      {data.sentiments && (
                        <Tooltip title="Click here to view sentiment analysis report of this audio." placement="top">
                          <Link to={`/app/view/${data._id}`}>
                            <Button>
                              <VisibilityIcon color="secondary" />
                            </Button>
                          </Link>
                        </Tooltip>
                      )}
                    </TableCell>
                  </TableRow>
              ))}
            </TableBody>
          </Table>
          )}
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        page={page}
        rowsPerPageOptions={pages}
        rowsPerPage={rowsPerPage}
        count={files ? files.length : 0}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Card>
  );
};

export default Results;
