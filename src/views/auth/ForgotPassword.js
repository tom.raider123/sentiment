import React, { useState } from 'react';
import PropTypes from 'prop-types';
// import clsx from 'clsx';
import axios from 'axios';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  // makeStyles,
  Container
} from '@material-ui/core';
import { isEmail } from '../../utils/validations/Validation';
import { showErrMsg, showSuccessMsg } from '../../utils/notification/Notification';

// const useStyles = makeStyles({
//   root: {}
// });

const initialState = {
  email: '',
  err: '',
  success: ''
};

const Password = ({ className, ...rest }) => {
  const [data, setData] = useState(initialState);
  const { email, err, success } = data;

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setData({
      ...data, [name]: value, err: '', success: ''
    });
  };

  const forgotPassword = async () => {
    if (!isEmail(email)) {
      return setData({ ...data, err: 'Invalid emails.', success: '' });
    }
    try {
      const res = await axios.post('/user/forgot', { email });
      return setData({ ...data, err: '', success: res.data.msg });
    } catch (error) {
      setData({ ...data, err: error.response.data.msg, success: '' });
    }
    return '';
  };

  return (
    <Container component="main" maxWidth="xs">
      <form {...rest}>
        <Card>
          <CardHeader title="FORGOT YOUR PASSWORD?" />
          <Divider />
          {err && showErrMsg(err)}
          {success && showSuccessMsg(success)}
          <CardContent>
            <TextField
              fullWidth
              label="Enter your email address"
              margin="normal"
              name="email"
              onChange={handleChangeInput}
              type="email"
              value={email}
              variant="outlined"
            />
          </CardContent>
          <Divider />
          <Box display="flex" justifyContent="flex-end" p={2}>
            <Button
              color="primary"
              variant="contained"
              onClick={forgotPassword}
            >
              Verify your email
            </Button>
          </Box>
        </Card>
      </form>
    </Container>
  );
};

Password.propTypes = {
  className: PropTypes.string
};

export default Password;
