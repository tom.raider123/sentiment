import React, { useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  // makeStyles,
  Container
} from '@material-ui/core';

import {
  showErrMsg,
  showSuccessMsg
} from '../../utils/notification/Notification';
import { isLength, isMatch } from '../../utils/validations/Validation';
// const useStyles = makeStyles({
//   root: {}
// });

const initialState = {
  password: '',
  confirmPassword: '',
  err: '',
  success: ''
};

const Password = ({ className, ...rest }) => {
  const [data, setData] = useState(initialState);
  const { token } = useParams();
  const {
    password, confirmPassword, err, success
  } = data;
  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setData({
      ...data,
      [name]: value,
      err: '',
      success: ''
    });
  };

  const handleResetPass = async () => {
    if (isLength(password)) {
      return setData({
        ...data,
        err: 'Password must be at least 6 characters.',
        success: ''
      });
    }
    if (!isMatch(password, confirmPassword)) {
      return setData({ ...data, err: 'Password did not match.', success: '' });
    }
    try {
      const res = await axios.post(
        '/user/reset',
        { password },
        {
          headers: { Authorization: token }
        }
      );
      return setData({ ...data, err: '', success: res.data.msg });
    } catch (error) {
      setData({ ...data, err: error.response.data.msg, success: '' });
    }
    return '';
  };

  return (
    <Container component="main" maxWidth="xs">
      <form {...rest}>
        <Card>
          <CardHeader title="Reset password" />
          <Divider />
          {err && showErrMsg(err)}
          {success && showSuccessMsg(success)}
          <CardContent>
            <TextField
              fullWidth
              label="Password"
              margin="normal"
              name="password"
              onChange={handleChangeInput}
              type="password"
              value={password}
              variant="outlined"
            />
            <TextField
              fullWidth
              label="Confirm password"
              margin="normal"
              name="confirmPassword"
              onChange={handleChangeInput}
              type="password"
              value={confirmPassword}
              variant="outlined"
            />
          </CardContent>
          <Divider />
          <Box display="flex" justifyContent="flex-end" p={2}>
            <Button
              color="primary"
              variant="contained"
              onClick={handleResetPass}
            >
              Reset Password
            </Button>
          </Box>
        </Card>
      </form>
    </Container>
  );
};

Password.propTypes = {
  className: PropTypes.string
};

export default Password;
