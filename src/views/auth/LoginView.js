import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import Alert from '@material-ui/lab/Alert';
import {
  Avatar,
  Button,
  Card,
  Grid,
  Typography,
  Container
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { dispatchLogin } from '../../redux/actions/authAction';
import useStyles from './styles';
import Input from './Input';
// import { signIn } from 'src/api';

const initialState = {
  email: '',
  password: '',
  err: '',
  success: ''
};
const SignIn = () => {
  const [user, setUser] = useState(initialState);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const classes = useStyles();
  const {
    email, password, err, success
  } = user;
  const [showPassword, setShowPassword] = useState(false);
  const handleShowPassword = () => setShowPassword(!showPassword);
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post('/user/login', { email, password });
      setUser({ ...user, err: '', success: res.data.msg });
      localStorage.setItem('firstLogin', true);
      dispatch(dispatchLogin());
      navigate('/app/dashboard');
    } catch (error) {
      setUser({ ...user, err: error.response.data.msg, success: '' });
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({
      ...user, [name]: value, err: '', success: ''
    });
  };
  return (
    <Container component="main" maxWidth="xs">
      <Card className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Grid item xs={12}>
          {err && <Alert severity="error">{err}</Alert>}
          {success && <Alert severity="success">{success}</Alert>}
        </Grid>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} />
            <Input
              name="email"
              value={email}
              label="Email Address"
              handleChange={handleChange}
              type="email"
            />
            <Input
              name="password"
              value={password}
              label="Password"
              handleChange={handleChange}
              type={showPassword ? 'text' : 'password'}
              handleShowPassword={handleShowPassword}
            />
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            signIn
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Button>
                Do not have an account?
                <Link to="/register">Register</Link>
              </Button>
            </Grid>
          </Grid>
        </form>
      </Card>
    </Container>
  );
};

export default SignIn;
