import React, { useState, useEffect } from 'react';
import { useParams, Link as RouterLink } from 'react-router-dom';
import axios from 'axios';
import { Button, Container } from '@material-ui/core';
import {
  showErrMsg,
  showSuccessMsg
} from '../../utils/notification/Notification';

function ActivationEmail() {
  const { activationToken } = useParams();
  const [err, setErr] = useState('');
  const [success, setSuccess] = useState('');

  useEffect(() => {
    if (activationToken) {
      const activationEmail = async () => {
        try {
          const res = await axios.post('/user/activation', {
            activationToken
          });
          setSuccess(res.data.msg);
        } catch (error) {
          setErr(error.response.data.msg);
        }
      };
      activationEmail();
    }
  }, [activationToken]);

  return (
    <Container component="main" maxWidth="xs">
      {err && showErrMsg(err)}
      {success && showSuccessMsg(success)}
      {success && (
        <RouterLink to="/">
          <Button fullWidth variant="contained" color="primary">
            Click here to login!!
          </Button>
        </RouterLink>
      )}
      {err && (
        <RouterLink to="/register">
          <Button fullWidth variant="contained" color="primary">
            Click here to register!!
          </Button>
        </RouterLink>
      )}
    </Container>
  );
}

export default ActivationEmail;
