import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import {
  Avatar, Button, Card, Grid, Typography, Container
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import useStyles from './styles';
import Input from './Input';

import {
  isEmpty, isEmail, isLength, isMatch
} from '../../utils/validations/Validation';

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  confirmPassword: '',
  err: '',
  success: ''
};

const SignUp = () => {
  const [user, setUser] = useState(initialState);
  const {
    firstName, lastName, email, password, confirmPassword, err, success
  } = user;
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);
  const handleShowPassword = () => setShowPassword(!showPassword);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({
      ...user, [name]: value, err: '', success: ''
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (isEmpty(firstName) || isEmpty(lastName) || isEmpty(email) || isEmpty(password)) {
      return setUser({ ...user, err: 'Please fill in all fields.', success: '' });
    }
    if (!isEmail(email)) {
      return setUser({ ...user, err: 'Invalid emails.', success: '' });
    }
    if (isLength(password)) {
      return setUser({ ...user, err: 'Password must be at least 6 characters.', success: '' });
    }
    if (!isMatch(password, confirmPassword)) {
      return setUser({ ...user, err: 'Password did not match.', success: '' });
    }
    try {
      const res = await axios.post('/user/register', {
        firstName, lastName, email, password
      });
      setUser({ ...user, err: '', success: res.data.msg });
    } catch (error) {
      setUser({ ...user, err: error.response.data.msg, success: '' });
    }
    return '';
  };

  return (
    <Container component="main" maxWidth="xs">
      <Card className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">Sign up</Typography>
        <Grid item xs={12}>
          {err && <Alert severity="error">{err}</Alert>}
          {success && <Alert severity="success">{success}</Alert>}
        </Grid>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Input name="firstName" label="First Name" handleChange={handleChange} autoFocus half />
            <Input name="lastName" label="Last Name" handleChange={handleChange} half />
            <Input name="email" label="Email Address" handleChange={handleChange} type="email" />
            <Input name="password" label="Password" handleChange={handleChange} type={showPassword ? 'text' : 'password'} handleShowPassword={handleShowPassword} />
            <Input name="confirmPassword" label="Repeat Password" handleChange={handleChange} type="password" />
          </Grid>
          <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Button>
                <p>
                  Already an account?
                  <Link to="/">Login</Link>
                </p>
              </Button>
            </Grid>
          </Grid>
        </form>
      </Card>
    </Container>
  );
};

export default SignUp;
