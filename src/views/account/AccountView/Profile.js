import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  Typography,
  makeStyles
} from '@material-ui/core';
import {
  showSuccessMsg,
  showErrMsg
} from '../../../utils/notification/Notification';
import { updateAvatar } from '../../../redux/actions/usersAction';
import Spinner from '../../../utils/loader/Spinner';

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    height: 100,
    width: 100
  }
  // input: {
  //   display: 'none',
  // },
}));

const initialState = {
  err: '',
  success: ''
};

const Profile = ({ className, ...rest }) => {
  const classes = useStyles();
  const auth = useSelector((state) => state.auth);
  const token = useSelector((state) => state.token);
  const updateavatar = useSelector((state) => state.updateUserAvatar);
  const { loading: uploadLoading, error: uploadError } = updateavatar;

  const { user } = auth;
  const [data, setData] = useState(initialState);
  const { err, success } = data;
  const [avatar, setAvatar] = useState(false);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  const changeAvatar = async (e) => {
    e.preventDefault();
    try {
      const file = e.target.files[0];
      if (!file) {
        return setData({
          ...data,
          err: 'No files were uploaded.',
          success: ''
        });
      }
      if (file.size > 1024 * 1024) {
        return setData({ ...data, err: 'Size too large.', success: '' });
      }
      if (file.type !== 'image/jpeg' && file.type !== 'image/png') {
        return setData({
          ...data,
          err: 'File format is incorrect.',
          success: ''
        });
      }
      const formData = new FormData();
      formData.append('file', file);
      setLoading(true);
      const res = await axios.post('/api/upload_avatar', formData, {
        headers: { 'content-type': 'multipart/form-data', Authorization: token }
      });
      setLoading(false);
      setAvatar(res.data.url);
      if (res.data.url) {
        // updateAvatar(avatar || res.data.url);
        dispatch(updateAvatar(user._id, token, { ...avatar, avatar: res.data.url || user.avatar }));
      }
    } catch (error) {
      setData({ ...data, err: error.response.data.msg, success: '' });
    }
    return '';
  };

  // const handleUpdate = () => {
  //   if (avatar) {
  //     updateInfor();
  //   }
  // };

  return (
    <>
      {err && showErrMsg(err)}
      {success && showSuccessMsg(success)}
      {loading && <h3><Spinner /></h3>}
      {uploadLoading && <Spinner />}
      {uploadError && uploadError}
      <Card className={clsx(classes.root, className)} {...rest}>
        <CardContent>
          <Box alignItems="center" display="flex" flexDirection="column">
            <Avatar className={classes.avatar} src={avatar || user.avatar} />
            <Typography color="textPrimary" gutterBottom variant="h3">
              {user.firstName}
              {' '}
              {user.lastName}
            </Typography>
            {/* {avatar && updateInfor()} */}
            <Typography color="textSecondary" variant="body1">
              {`${user.country ? user.country : ''} ${
                user.city ? user.city : ''
              }`}
            </Typography>
            <Typography
              className={classes.dateText}
              color="textSecondary"
              variant="body1"
            >
              {`${moment().format('hh:mm A')} GMT+5:30`}
            </Typography>
          </Box>
        </CardContent>
        <Divider />
        <CardActions>
          <Button color="primary" className={classes.input} accept="image/*" fullWidth variant="text" component="label">
            Upload picture
            <input type="file" multiple hidden name="avatar" onChange={changeAvatar} />
          </Button>
        </CardActions>
      </Card>
    </>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
