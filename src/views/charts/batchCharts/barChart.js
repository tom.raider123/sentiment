import React from 'react';
import { Bar } from 'react-chartjs-2';
import PropTypes from 'prop-types';

const GroupedBar = (props) => {
  const { staticsData } = props;
  const converted = staticsData.filter((data) => data.isConverted === 1);
  const notConverted = staticsData.filter((data) => data.isConverted === 0);
  const analysed = staticsData.filter((data) => data.isAnalysed === 1);
  const notAnalysed = staticsData.filter((data) => data.isAnalysed === 0);
  const positive = staticsData.filter((data) => data.sentiments.score > 0);
  const negative = staticsData.filter((data) => data.sentiments.score < 0);
  const neutral = staticsData.filter((data) => data.sentiments.score === 0);
  const data = {
    labels: ['Analysis'],
    datasets: [
      {
        label: '# Converted',
        data: [converted.length, staticsData.length],
        backgroundColor: 'rgb(75, 192, 192)'
      },
      {
        label: '# Not Converted',
        data: [notConverted.length, staticsData.length],
        backgroundColor: 'rgb(100,149,237)'
      },
      {
        label: '# Analysed',
        data: [analysed.length, staticsData.length],
        backgroundColor: 'rgb(54, 162, 235)'
      },
      {
        label: '# Not analysed',
        data: [notAnalysed.length, staticsData.length],
        backgroundColor: 'rgb(186,85,211)'
      },
      {
        label: '# Positive',
        data: [positive.length, staticsData.length],
        backgroundColor: 'rgb(0,0,139)'
      },
      {
        label: '# Negative',
        data: [negative.length, staticsData.length],
        backgroundColor: 'rgb(199,21,133))'
      },
      {
        label: '# Neutral',
        data: [neutral.length, staticsData.length],
        backgroundColor: 'rgb(255,105,180)'
      },
    ]
  };
  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  };
  return (
    <div>
      <Bar data={data} options={options} />
    </div>
  );
};

GroupedBar.propTypes = {
  staticsData: PropTypes.any,
};
export default GroupedBar;
