import React from 'react';
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types';

const MultiAxisLine = (props) => {
  const { staticsData } = props;
  const converted = staticsData.filter((data) => data.isConverted === 1);
  const notConverted = staticsData.filter((data) => data.isConverted === 0);
  const analysed = staticsData.filter((data) => data.isAnalysed === 1);
  const notAnalysed = staticsData.filter((data) => data.isAnalysed === 0);
  const positive = staticsData.filter((data) => data.sentiments.score > 0);
  const negative = staticsData.filter((data) => data.sentiments.score < 0);
  const neutral = staticsData.filter((data) => data.sentiments.score === 0);
  const data = {
    labels: ['Processed', 'Total'],
    datasets: [
      {
        label: '# Converted',
        data: [converted.length, staticsData.length],
        fill: false,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgba(255, 99, 132, 0.2)',
        yAxisID: 'y-axis-1'
      },
      {
        label: '# Not converted',
        data: [notConverted.length, staticsData.length],
        fill: false,
        backgroundColor: 'rgb(64,224,208)',
        borderColor: 'rgba(64,224,208,0.2)',
        yAxisID: 'y-axis-2'
      },
      {
        label: '# Analysed',
        data: [analysed.length, staticsData.length],
        fill: false,
        backgroundColor: 'rgb(30,144,255)',
        borderColor: 'rgba(30,144,255, 0.2)',
        yAxisID: 'y-axis-1'
      },
      {
        label: '# Not analysed',
        data: [notAnalysed.length, staticsData.length],
        fill: false,
        backgroundColor: 'rgb(54, 162, 235)',
        borderColor: 'rgba(54, 162, 235, 0.2)',
        yAxisID: 'y-axis-2'
      },
      {
        label: '# Positive',
        data: [positive.length, staticsData.length],
        fill: false,
        backgroundColor: 'rgb(139,69,19)',
        borderColor: 'rgba(139,69,19,0.2)',
        yAxisID: 'y-axis-1'
      },
      {
        label: '# Negative',
        data: [negative.length, staticsData.length],
        fill: false,
        backgroundColor: 'rgb(220,20,60)',
        borderColor: 'rgba(220,20,60,0.2)',
        yAxisID: 'y-axis-2'
      },
      {
        label: '# Neutral',
        data: [neutral.length, staticsData.length],
        fill: false,
        backgroundColor: 'rgb(0,206,209)',
        borderColor: 'rgba(0,206,209,0.2)',
        yAxisID: 'y-axis-2'
      }

    ]
  };
  const options = {
    scales: {
      yAxes: [
        {
          type: 'linear',
          display: true,
          position: 'left',
          id: 'y-axis-1'
        },
        {
          type: 'linear',
          display: true,
          position: 'right',
          id: 'y-axis-2',
          gridLines: {
            drawOnArea: false
          }
        }
      ]
    }
  };
  return (
    <div>
      <Line data={data} options={options} />
    </div>
  );
};

MultiAxisLine.propTypes = {
  staticsData: PropTypes.any,
};
export default MultiAxisLine;
