import { colors } from '@material-ui/core';
import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import PropTypes from 'prop-types';

function doughnutChart(props) {
  const { staticsData } = props;
  const converted = staticsData.filter((data) => data.isConverted === 1);
  const notConverted = staticsData.filter((data) => data.isConverted === 0);
  const analysed = staticsData.filter((data) => data.isAnalysed === 1);
  const notAnalysed = staticsData.filter((data) => data.isAnalysed === 0);
  const positive = staticsData.filter((data) => data.sentiments.score > 0);
  const negative = staticsData.filter((data) => data.sentiments.score < 0);
  const neutral = staticsData.filter((data) => data.sentiments.score === 0);
  const data = {
    datasets: [
      {
        data: [
          converted.length,
          notConverted.length,
          analysed.length,
          notAnalysed.length,
          positive.length,
          negative.length,
          neutral.length
        ],
        backgroundColor: [
          colors.teal[500],
          colors.pink[500],
          colors.deepPurple[500],
          colors.deepOrange[500],
          colors.red[500],
          colors.purple[500],
          colors.green[500],
        ],
        borderWidth: 1,
        borderColor: colors.common.white,
        hoverBorderColor: colors.common.white
      }
    ],
    labels: ['Converted', 'Not converted', 'Analysed', 'Not analysed', 'Positive', 'Negative', 'Neutral']
  };
  return (
    <div>
      <Doughnut data={data} />
    </div>
  );
}

doughnutChart.propTypes = {
  staticsData: PropTypes.any,
};
export default doughnutChart;
