import { colors } from '@material-ui/core';
import React from 'react';
import { Pie } from 'react-chartjs-2';
import PropTypes from 'prop-types';

const PieChart = (props) => {
  const { staticsData } = props;
  const converted = staticsData.filter((data) => data.isConverted === 1);
  const notConverted = staticsData.filter((data) => data.isConverted === 0);
  const analysed = staticsData.filter((data) => data.isAnalysed === 1);
  const notAnalysed = staticsData.filter((data) => data.isAnalysed === 0);
  const positive = staticsData.filter((data) => data.sentiments.score > 0);
  const negative = staticsData.filter((data) => data.sentiments.score < 0);
  const neutral = staticsData.filter((data) => data.sentiments.score === 0);
  const data = {
    datasets: [
      {
        data: [
          converted.length,
          notConverted.length,
          analysed.length,
          notAnalysed.length,
          positive.length,
          negative.length,
          neutral.length
        ],
        backgroundColor: [
          colors.lightBlue[500],
          colors.indigo[500],
          colors.blueGrey[500],
          colors.orange[500],
          colors.cyan[500],
          colors.pink[700],
          colors.deepPurple[500],
        ],

        borderWidth: 1,
        borderColor: colors.common.white,
        hoverBorderColor: colors.common.white
      }
    ],
    labels: ['Converted', 'Not converted', 'Analysed', 'Not analysed', 'Positive', 'Negative', 'Neutral']
  };
  return (
    <div>
      <Pie data={data} />
    </div>
  );
};

PieChart.propTypes = {
  staticsData: PropTypes.any
};
export default PieChart;
