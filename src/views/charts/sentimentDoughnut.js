import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Doughnut } from 'react-chartjs-2';
import {
  Box,
  Card,
  CardContent,
  Divider,
  colors,
  makeStyles,
//   useTheme
} from '@material-ui/core';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(() => ({
  root: {
    height: '100%'
  }
}));

const SentimentDoughnut = ({ className, ...rest }) => {
  const classes = useStyles();
  const details = useSelector((state) => state.getFilesById);
  const { file } = details;
  //   const theme = useTheme();
  const PositiveCount = file && file.data.positive.length;
  const NegativeCount = file && file.data.negative.length;
  const data = {
    datasets: [
      {
        data: [PositiveCount, NegativeCount],
        backgroundColor: [
          colors.green[500],
          colors.red[600],
        ],
        borderWidth: 8,
        borderColor: colors.common.white,
        hoverBorderColor: colors.common.white
      }
    ],
    labels: ['Positive', 'Negative']
  };

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Divider />
      <CardContent>
        <Box
          height={400}
          position="relative"
        >
          <Doughnut
            data={data}
          />
        </Box>
      </CardContent>
    </Card>
  );
};

SentimentDoughnut.propTypes = {
  className: PropTypes.string,
  props: PropTypes.string
};

export default SentimentDoughnut;
