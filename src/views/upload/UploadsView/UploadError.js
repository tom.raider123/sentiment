import {
  createStyles,
  LinearProgress,
  Typography,
  withStyles
} from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';
// import { FileError } from 'react-dropzone';
import FileHeader from './FileHeader';

const ErrorLinearProgress = withStyles((theme) => createStyles({
  bar: {
    backgroundColor: theme.palette.error.main
  }
}))(LinearProgress);

function UploadError(props) {
  const { file, onDelete, errors } = props;

  return (
    <>
      <FileHeader file={file} onDelete={onDelete} />
      <ErrorLinearProgress variant="determinate" value={100} />
      {errors.map((error) => (
        <div key={error.code}>
          <Typography color="error">{error.message}</Typography>
        </div>
      ))}
    </>
  );
}

UploadError.propTypes = {
  file: PropTypes.any,
  onDelete: PropTypes.func,
  errors: PropTypes.array
};

export default UploadError;
