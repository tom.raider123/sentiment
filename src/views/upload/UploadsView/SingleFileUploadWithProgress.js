import { Grid, LinearProgress } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import FileHeader from './FileHeader';
// import { useDispatch } from 'react-redux';

function uploadFile(file, onProgress) {
  // const url = 'https://api.cloudinary.com/v1_1/demo/image/upload';
  // const key = 'docs_upload_example_us_preset';
  const url = '/api/upload_audio_cloud';
  return new Promise((res, rej) => {
    const xhr = new XMLHttpRequest();
    xhr.open('POST', url);

    xhr.onload = () => {
      const resp = JSON.parse(xhr.responseText);
      res(resp);
    };
    xhr.onerror = (evt) => rej(evt);
    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        const percentage = (event.loaded / event.total) * 100;
        onProgress(Math.round(percentage));
      }
    };
    const formData = new FormData();
    formData.append('file', file);
    // formData.append('upload_preset', key);
    xhr.send(formData);
    // console.log(formData);
  });
}
function SingleFileUploadWithProgress({ file, onDelete, onUpload }) {
  const [progress, setProgress] = useState(0);
  useEffect(() => {
    async function upload() {
      const url = await uploadFile(file, setProgress);
      onUpload(file, url);
    }
    upload();
  }, []);

  return (
    <Grid item>
      <FileHeader file={file} onDelete={onDelete} />
      <LinearProgress variant="determinate" value={progress} />
    </Grid>
  );
}

SingleFileUploadWithProgress.propTypes = {
  file: PropTypes.any,
  onDelete: PropTypes.any,
  onUpload: PropTypes.any
};

uploadFile.propTypes = {
  file: PropTypes.string,
  onProgress: PropTypes.any,
  percentage: PropTypes.number
};
export default SingleFileUploadWithProgress;
