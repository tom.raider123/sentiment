import React, { useEffect, useState, useCallback } from 'react';
import { Grid, makeStyles } from '@material-ui/core';
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import { useDropzone } from 'react-dropzone';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import SingleFileUploadWithProgress from './SingleFileUploadWithProgress';
import UploadError from './UploadError';

const useStyles = makeStyles((theme) => ({
  dropzone: {
    border: `2px dashed ${theme.palette.primary.main}`,
    borderRadius: theme.shape.borderRadius,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    background: theme.palette.background.default,
    height: theme.spacing(10),
    outline: 'none'
  }
}));

function MultipleFileUploadField(props) {
  const { name } = props;
  const [, , helpers] = useField(name);
  // const { setValue } = helpers;
  const classes = useStyles();
  const [files, setFiles] = useState([]);
  const onDrop = useCallback((accFiles, rejFiles) => {
    const mappedAcc = accFiles.map((file) => ({ file, errors: [] }));
    setFiles((curr) => [...curr, ...mappedAcc, ...rejFiles]);
  }, []);
  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: ['.wav'],
    maxSize: 300000 * 1024 // 300KB
  });

  useEffect(() => {
    helpers.setValue(files);
    // helpers.setTouched(true);
  }, [files]);

  function onUpload(file, url) {
    setFiles((curr) => curr.map((fw) => {
      if (fw.file === file) {
        return { ...fw, url };
      }
      return fw;
    }));
  }

  function onDelete(file) {
    setFiles((curr) => curr.filter((fw) => fw.file !== file));
  }

  return (
    <>
      <Grid item>
        <div {...getRootProps({ className: classes.dropzone })}>
          <input {...getInputProps()} />
          <CloudUploadIcon color="primary" />
          <p>Drag and drop some files and folder here, or click to select files..</p>
        </div>
      </Grid>
      {files.map((fileWrapper, id = uuidv4()) => (
        <Grid item key={id}>
          {fileWrapper.errors.length ? (
            <UploadError
              file={fileWrapper.file}
              errors={fileWrapper.errors}
              onDelete={onDelete}
            />
          ) : (
            <SingleFileUploadWithProgress
              onDelete={onDelete}
              onUpload={onUpload}
              file={fileWrapper.file}
            />
          )}
        </Grid>
      ))}
    </>
  );
}
MultipleFileUploadField.propTypes = {
  name: PropTypes.string,
  errors: PropTypes.array,
  url: PropTypes.string
};

export default MultipleFileUploadField;
