import React from 'react';
import { array, object, string } from 'yup';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { Form, Formik } from 'formik';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Grid
  // makeStyles
} from '@material-ui/core';
import MultipleFileUploadField from './MultipleFileUploadField';
import { uploadFiles } from '../../../redux/actions/uploadFiles';
import SimpleBackdrop from '../../../utils/loader/BackDrop';
import { showSuccessSnackbar } from '../../../redux/actions/snackbarActions';
import useUnsavedChangesWarning from '../../../customHooks/useUnsavedChangesWarning';

uuidv4();
const Upload = () => {
  const auth = useSelector((state) => state.auth);
  const token = useSelector((state) => state.token);
  const uploadstatus = useSelector((state) => state.uploadUserFiles);
  const { loading, error } = uploadstatus;
  const [Prompt, setDirty, setPristine] = useUnsavedChangesWarning();

  // if (uploads) {
  //   JSON.stringify(uploads.data, null, 4);
  //   // console.log(uploads.data);
  // }
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleRedirect = () => {
    navigate('/app/customers');
  };
  return (
    <Formik
      initialValues={{
        files: [],
        batchName: ''
      }}
      validationSchema={object({
        files: array(
          object({
            url: string().required()
          })
        ).required(),
        // batchName: string().required('Please enter batch name.'),
        batchName: string()
          .min(3, 'Too short, minimum 2 characters')
          .max(50, 'Too long, maximum 50 characters')
          .required('Please enter batch name.')
          .test(
            'is_valid',
            'Batch name already been taken.',
            (batchName) => {
              return new Promise((resolve, reject) => {
                axios
                  .post('/user/validatebatchname/', {
                    batchName,
                    headers: { Authorization: token }
                  })
                  .then((res) => {
                    if (res.data.msg === 'Batch name already been taken') {
                      resolve(false);
                    }
                    resolve(true);
                  });
                console.log(reject);
              });
            }
          )
      })}
      onSubmit={(values) => {
        setPristine();
        if (auth.user._id !== '') {
          dispatch(uploadFiles(values, token, auth ? auth.user._id : null));
        }
      }}
    >
      {({
        values,
        errors,
        touched,
        isValid,
        isSubmitting,
        handleBlur,
        handleChange
      }) => (
        <Form>
          <Card>
            <CardHeader subheader="Upload" title="Upload" />
            {loading && <SimpleBackdrop value={loading} />}
            {error && dispatch(showSuccessSnackbar(error))}
            <Divider />
            <Box display="flex" justifyContent="flex-start" p={2}>
              <Grid item md={6} xs={12}>
                <TextField
                  name="batchName"
                  // value={values.batchName}
                  label="Batch Name"
                  size="small"
                  variant="outlined"
                  onChange={(e) => {
                    handleChange(e);
                    setDirty();
                  }}
                  onBlur={handleBlur}
                  error={Boolean(touched.batchName && errors.batchName)}
                  helperText={touched.batchName && errors.batchName}
                  autoFocus
                />
              </Grid>
            </Box>
            <Divider />
            <CardContent>
              <MultipleFileUploadField name="files" />
            </CardContent>
            <Divider />
            <Box display="flex" justifyContent="flex-end" p={2}>
              <Button onClick={handleRedirect}>Back</Button>
              <Button
                color="primary"
                variant="contained"
                type="submit"
                disabled={!isValid || isSubmitting}
              >
                Upload
              </Button>
            </Box>
            {Prompt}
            {console.log({ values, errors })}
            {/* <pre>{JSON.stringify({ values, errors }, null, 4)}</pre> */}
          </Card>
        </Form>
      )}
    </Formik>
  );
};

export default Upload;
