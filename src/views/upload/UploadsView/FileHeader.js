import { Button, Grid } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';

function FileHeader(props) {
  const { file, onDelete } = props;
  return (
    <Grid container justify="space-between" alignItems="center">
      <Grid item>{file.name}</Grid>
      <Grid item>
        <Button size="small" onClick={() => onDelete(file)}>
          Delete
        </Button>
      </Grid>
    </Grid>
  );
}

FileHeader.propTypes = {
  file: PropTypes.any,
  onDelete: PropTypes.func
};
export default FileHeader;
