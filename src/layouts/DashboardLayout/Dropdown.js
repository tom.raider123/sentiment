import React from 'react';
import { useSelector } from 'react-redux';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link as RouterLink, useNavigate } from 'react-router-dom';

export default function SimpleMenu() {
  const navigate = useNavigate();
  const auth = useSelector((state) => state.auth);
  const { user, isLogged } = auth;

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleLogout = async () => {
    try {
      await axios.get('/user/logout');
      localStorage.removeItem('firstLogin');
      window.location = '/login';
    } catch (err) {
      navigate('/login');
    }
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <span style={{ color: 'white' }}>{isLogged ? user.firstName : ''}</span>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <RouterLink to="/app/account">
          <MenuItem onClick={handleClose}>Profile</MenuItem>
        </RouterLink>
        {/* <RouterLink to="/app/settings">
          <MenuItem onClick={handleClose}>Setting</MenuItem>
        </RouterLink> */}
        <MenuItem onClick={handleLogout}>Logout</MenuItem>
      </Menu>
    </div>
  );
}
